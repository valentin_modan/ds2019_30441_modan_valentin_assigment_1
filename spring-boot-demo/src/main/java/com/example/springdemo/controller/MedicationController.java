package com.example.springdemo.controller;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id){
        return medicationService.findMedicationById(id);
    }

    @GetMapping()
    public List<MedicationDTO> findAll(){
        return medicationService.findAll();
    }

    @PostMapping()
    public Integer insertMedicationDTO(@RequestBody MedicationDTO personDTO){
        return medicationService.insert(personDTO);
    }

    @PutMapping()
    public Integer updateMedication(@RequestBody MedicationDTO personDTO) {
        return medicationService.update(personDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody MedicationDTO personViewDTO){
        medicationService.delete(personViewDTO);
    }


}
