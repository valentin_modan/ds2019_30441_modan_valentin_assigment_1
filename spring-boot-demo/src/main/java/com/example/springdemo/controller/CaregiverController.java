package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.services.CaregiverService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public CaregiverViewDTO findById(@PathVariable("id") Integer id) { return caregiverService.findCaregiverById(id);}

    @GetMapping()
    public List<CaregiverViewDTO> findAll() { return caregiverService.findAll();}

    @PostMapping()
    public Integer insertCaregiverDTO(@RequestBody CaregiverViewDTO caregiverViewDTO){
        return caregiverService.insert(caregiverViewDTO);

    }


    @PutMapping()
    public Integer updatePatient(@RequestBody CaregiverViewDTO patientDTO) {
        return caregiverService.update(patientDTO);
    }

    @DeleteMapping
    public void delete(@RequestBody CaregiverViewDTO patientDTO) {
        caregiverService.delete(patientDTO);
    }

}
