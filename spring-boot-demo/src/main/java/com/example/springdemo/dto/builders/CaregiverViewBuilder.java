package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverViewBuilder {
    private CaregiverViewBuilder(){}


    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.getGender(),
                caregiver.getAddress());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(
                caregiverViewDTO.getName(),
                caregiverViewDTO.getBirthDate(),
                caregiverViewDTO.getGender(),
                caregiverViewDTO.getAddress(),
                null,
                null
        );
    }
}
