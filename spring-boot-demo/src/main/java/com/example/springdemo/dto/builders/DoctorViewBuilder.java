package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorViewDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorViewBuilder {
    private DoctorViewBuilder(){}

    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(doctor.getId(),
                doctor.getName(),
                doctor.getEmail());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(
                doctorViewDTO.getId(),
                doctorViewDTO.getName(),
                doctorViewDTO.getEmail()
        );
    }
}
