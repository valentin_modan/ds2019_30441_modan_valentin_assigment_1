package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;

import java.util.Objects;

public class PatientDTO {

    private Integer id;
    private String name;
    private String email;
    private Caregiver caregiver;

    public PatientDTO() {
    }

    public PatientDTO(Integer id, String name, String email, Caregiver caregiver) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.caregiver = caregiver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO that = (PatientDTO) o;
        return name.equals(that.name) &&
                email.equals(that.email) &&
                Objects.equals(caregiver, that.caregiver);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, caregiver);
    }
}
