package com.example.springdemo.dto;

import java.util.Objects;

public class MedicationDTO {

    private Integer id;
    private String name;
    private String sideEffects;
    private String dosage;


    public MedicationDTO() {
    }

    public MedicationDTO(Integer id, String name, String sideEffects, String dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO that = (MedicationDTO) o;
        return name.equals(that.name) &&
                sideEffects.equals(that.sideEffects) &&
                dosage.equals(that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }

    @Override
    public String toString() {
        return "MedicationDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sideEffects='" + sideEffects + '\'' +
                ", dosage='" + dosage + '\'' +
                '}';
    }
}
