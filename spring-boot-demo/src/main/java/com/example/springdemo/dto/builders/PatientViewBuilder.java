package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;

public class PatientViewBuilder {
    private PatientViewBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        return new PatientDTO(patient.getId(),
                patient.getName(),
                patient.getEmail(),
                patient.getCaregiver());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getName(),
                patientDTO.getEmail(),
                patientDTO.getCaregiver());
    }
}
