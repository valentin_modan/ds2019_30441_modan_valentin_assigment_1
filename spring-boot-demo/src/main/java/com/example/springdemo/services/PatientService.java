package com.example.springdemo.services;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.PatientViewBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private final PatientRepository patientRepository;


    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientDTO findPatientByEmail(String email){
        Optional<Patient> patient  = patientRepository.findByEmail(email);


        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "email", email);
        }
        return PatientViewBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTO> findAll(){
        List<Patient> patientList = patientRepository.findAll();

        return patientList.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {

        Optional<Patient> person = patientRepository.findByEmail(patientDTO.getEmail());
        if(person.isPresent()){
            throw  new DuplicateEntryException("Patient", "email", patientDTO.getEmail());
        }

        System.out.println("Inserting person:" + patientDTO);

        return patientRepository
                .save(PatientViewBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> patient = patientRepository.findByEmail(patientDTO.getEmail());

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "user email", patientDTO.getEmail());
        }

        patient.get().setName(patientDTO.getName());

        return patientRepository.save(patient.get()).getId();
    }

    public void delete(PatientDTO personViewDTO){
        this.patientRepository.deleteByEmail(personViewDTO.getEmail());
    }
}
