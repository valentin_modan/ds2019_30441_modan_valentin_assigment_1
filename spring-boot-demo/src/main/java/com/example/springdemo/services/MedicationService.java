package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.builders.MedicationViewBuilder;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<MedicationDTO> findAll(){
        List<Medication> medicationList = medicationRepository.findAll();

        return medicationList.stream()
                .map(MedicationViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(Integer id) {
        Optional<Medication> medication = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", id);
        }
        return MedicationViewBuilder.generateDTOFromEntity(medication.get());
    }

    public MedicationDTO findMedicationByName(String name) {
        Optional<Medication> medication = medicationRepository.findMedicationByName(name);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication name", name);
        }

        return MedicationViewBuilder.generateDTOFromEntity(medication.get());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        Optional<Medication> medication = medicationRepository.findMedicationByName(medicationDTO.getName());

        if (medication.isPresent()) {
            throw new DuplicateEntryException("Medication", "name", medicationDTO.getName());
        }

        return medicationRepository
                .save(MedicationViewBuilder
                        .generateEntityFromMedication(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medicationDTO) {
        Optional<Medication> medication = medicationRepository.findMedicationByName(medicationDTO.getName());
        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", medicationDTO.getId().toString());
        }

        medication.get().setDosage(medicationDTO.getDosage());
        medication.get().setSideEffects(medicationDTO.getSideEffects());

        return medicationRepository.save(medication.get()).getId();
    }

    public void delete(MedicationDTO medicationDTO) {
        this.medicationRepository.deleteByName(medicationDTO.getName());
    }



}
