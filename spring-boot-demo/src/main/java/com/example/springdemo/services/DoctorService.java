package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorViewDTO;
import com.example.springdemo.dto.builders.DoctorViewBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorViewDTO findDoctorById(Integer id){
        Optional<Doctor> doctor = doctorRepository.findById(id);

        if(!doctor.isPresent())
        {
            throw new ResourceNotFoundException("Doctor", "doctor id", id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}
