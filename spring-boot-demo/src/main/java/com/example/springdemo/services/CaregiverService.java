package com.example.springdemo.services;


import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.dto.builders.CaregiverViewBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import org.omg.IOP.TAG_ALTERNATE_IIOP_ADDRESS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverViewDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);

            if(!caregiver.isPresent())
            {
                throw new ResourceNotFoundException("Caregiver", "caregiver id", id);
            }

            return CaregiverViewBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverViewDTO> findAll(){
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverViewDTO caregiverViewDTO)
    {
        Optional<Caregiver> caregiver = caregiverRepository.findByName(caregiverViewDTO.getName());

        if(caregiver.isPresent()) {
            throw new DuplicateEntryException("Caregiver", "name", caregiverViewDTO.getName());
        }
        return caregiverRepository
                .save(CaregiverViewBuilder.generateEntityFromDTO(caregiverViewDTO)).getId();
    }

    public Integer update(CaregiverViewDTO caregiverViewDTO)
    {
        Optional<Caregiver> caregiver = caregiverRepository.findByName(caregiverViewDTO.getName());

        if(!caregiver.isPresent())
        {
            throw new ResourceNotFoundException("Caregiver","caregiver name", caregiverViewDTO.getName());
        }

        caregiver.get().setAddress(caregiverViewDTO.getAddress());
        caregiver.get().setBirthDate(caregiverViewDTO.getBirthDate());
        caregiver.get().setGender(caregiverViewDTO.getGender());


        return caregiverRepository.save(caregiver.get()).getId();
    }

    public void delete(CaregiverViewDTO caregiverViewDTO){
        this.caregiverRepository.deleteByName(caregiverViewDTO.getName());
    }
}
