package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "email", unique = true, nullable = false, length = 100)
    private String email;

    @ManyToOne
    @JoinColumn(name = "caregiver")
    private Caregiver caregiver;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
    private Set<MedicationPlan> medicationPlan;

    public Patient() {
    }

    public Patient(String name, String email, Caregiver caregiver) {
        this.name = name;
        this.email = email;
        this.caregiver = caregiver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Set<MedicationPlan> getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(Set<MedicationPlan> medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(name, patient.name) &&
                Objects.equals(email, patient.email) &&
                Objects.equals(caregiver, patient.caregiver);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, caregiver);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", caregiver=" + caregiver +
                '}';
    }
}
