package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "caregiver")
public class Caregiver {
    // The doctor can perform CRUD operations on patient (defined by ID,
    //name, birth date, gender, address, medical record) and caregiver (defined by ID, name, birth
    //date, gender, address, list of patients taken care of) accounts and on the list of medication
    //(defined by ID, name, list of side effects, dosage) available in the system.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", unique = true, nullable = false, length = 100)
    private String name;

    @Column(name = "birthdate", nullable = false, length = 100)
    private String birthDate;

    @Column(name = "gender", nullable = false, length = 100)
    private String gender;

    @Column(name = "address", nullable = false, length = 100)
    private String address;

    //TODO: Make list of patients instead
    @Column(name = "patient", length = 100)
    private String patient;

    //TODO: Make list of medication instead
    @Column(name = "medication", length = 100)
    private String medication;

    //mapped by caregiver
    @OneToMany(mappedBy = "caregiver")
    private Set<Patient> patients;

    public Caregiver(String name, String birthDate, String gender, String address, String patient, String medication, Set<Patient> patients) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patient = patient;
        this.medication = medication;
        this.patients = patients;
    }

    public Caregiver(String name, String birthDate, String gender, String address, String patient, String medication) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patient = patient;
        this.medication = medication;
    }

    public Caregiver(String birthDate, String gender, String address, String patient) {
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patient = patient;
    }

    public Caregiver() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }
}
