package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver,Integer> {

    Optional<Caregiver> findByName(String name);

    @Transactional
    Optional<Caregiver> deleteByName(String name);
}
