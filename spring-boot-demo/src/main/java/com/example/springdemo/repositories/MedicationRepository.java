package com.example.springdemo.repositories;

import com.example.springdemo.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {

    Medication findAllBySideEffects(String sideEffects);

    Optional<Medication> findMedicationByName(String name);

    @Transactional
    Optional<Medication> deleteByName(String name);
}
