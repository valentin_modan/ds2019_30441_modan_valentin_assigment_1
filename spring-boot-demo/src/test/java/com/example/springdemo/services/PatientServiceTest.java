package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplicationTests;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PatientServiceTest extends SpringDemoApplicationTests {

    @Autowired
    PatientService patientService;

    @Test(expected = ResourceNotFoundException.class)
    public void testComplete() {
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setEmail("test@yahoo.com");
        patientDTO.setName("test");

        Integer id = patientService.insert(patientDTO);

        patientDTO.setId(id);

        PatientDTO copyPatientDTO = patientService.findPatientByEmail(patientDTO.getEmail());

        assert (patientDTO.equals(copyPatientDTO));

        //update

        patientDTO.setName("secondName");

        id = patientService.update(patientDTO);


        copyPatientDTO = patientService.findPatientByEmail(patientDTO.getEmail());

        assert patientDTO.equals(copyPatientDTO);

        patientService.delete(patientDTO);

        copyPatientDTO = patientService.findPatientByEmail(patientDTO.getEmail());

        assert (copyPatientDTO == null);

    }
}
