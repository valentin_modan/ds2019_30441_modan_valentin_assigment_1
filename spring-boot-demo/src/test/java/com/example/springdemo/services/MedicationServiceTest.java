package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplicationTests;
import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MedicationServiceTest extends SpringDemoApplicationTests {

    @Autowired MedicationService medicationService;

    @Test
    public void insertion_test()
    {
        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setName("Aspiring");
        medicationDTO.setDosage("1/week");
        medicationDTO.setSideEffects("rash");
        Integer id = medicationService.insert(medicationDTO);

        MedicationDTO medicationDTO1 = medicationService.findMedicationById(id);


        System.out.println(medicationDTO);
        System.out.println(medicationDTO1);

        assert(medicationDTO.equals(medicationDTO1));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void test_complete()
    {
         MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setName("XBOX");
        medicationDTO.setDosage("1/day");
        medicationDTO.setSideEffects("test");

        Integer id = medicationService.insert(medicationDTO);

        medicationDTO.setId(id);

        MedicationDTO copyMedicationDTO = medicationService.findMedicationById(id);

        assert(medicationDTO.equals(copyMedicationDTO));
        //update

        medicationDTO.setDosage("1/week");
        medicationDTO.setSideEffects("pizza");

        id = medicationService.update(medicationDTO);


        copyMedicationDTO = medicationService.findMedicationById(id);

        System.out.println(medicationDTO);
        System.out.println(copyMedicationDTO);
        assert(medicationDTO.equals(copyMedicationDTO));


         medicationService.delete(medicationDTO);

         copyMedicationDTO = medicationService.findMedicationById(id);

         assert(copyMedicationDTO==null);
    }
}
