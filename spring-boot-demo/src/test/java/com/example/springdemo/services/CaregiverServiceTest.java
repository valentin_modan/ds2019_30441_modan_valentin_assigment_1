package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplicationTests;
import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CaregiverServiceTest extends SpringDemoApplicationTests {

    @Autowired CaregiverService caregiverService;

    @Test(expected = ResourceNotFoundException.class)
    public void test_complete()
    {
        CaregiverViewDTO caregiverViewDTO = new CaregiverViewDTO("name","birth","gender","address");

        Integer id =caregiverService.insert(caregiverViewDTO);

        caregiverViewDTO.setId(id);

        CaregiverViewDTO copyCaregiverViewDTO = caregiverService.findCaregiverById(id);

        System.out.println(caregiverViewDTO);
        System.out.println(copyCaregiverViewDTO);
        assert(caregiverViewDTO.equals(copyCaregiverViewDTO));


        caregiverViewDTO.setAddress("floresti");
        caregiverService.update(caregiverViewDTO);

        copyCaregiverViewDTO = caregiverService.findCaregiverById(id);

        assert(caregiverViewDTO.equals(copyCaregiverViewDTO));

        caregiverService.delete(caregiverViewDTO);

        copyCaregiverViewDTO = caregiverService.findCaregiverById(id);
    }
}
