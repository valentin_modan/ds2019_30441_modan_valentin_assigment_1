import React from 'react';
import validate from "./validators/patient-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class PatientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                email: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerPatient(patient) {
        return API_USERS.postPatient(patient, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updatePatient(patient) {
        return API_USERS.putPatient(patient, (result, status, error) => {
            console.log(result);

            if (result != null && (status === 200 || status === 2010)) {
                console.log("Succesfully updated person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    deletePatient(patient) {
        return API_USERS.deletePatient(patient, (result, status, error) => {
            console.log(result);

            if (result != null && (status === 200 || status === 201)) {
                console.log("Succesfully delete patient with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }


    handleSubmit() {
        console.log("New patient data:");


        let user = {
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
        };

        this.registerPatient(user);
    }

    handleUpdate() {
        console.log("New person data:");
        let user = {
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
        };

        this.updatePatient(user);
    }

    handleDelete() {
        console.log("New person data:");
        let user = {
            email: this.state.formControls.email.value,
        };

        this.deletePatient(user);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>Insert new person</h1>

                <p> Email: </p>
                <TextInput name="email"
                           placeholder={this.state.formControls.email.placeholder}
                           value={this.state.formControls.email.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.email.touched}
                           valid={this.state.formControls.email.valid}
                />

                <p> Name: </p>

                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />


                <p></p>
                <Button variant="success"
                        type={"submit"}
                        value="insert">

                    Submit
                </Button>

                <br></br>
                <Button name="update_1"
                        onClick={() => this.handleUpdate()}
                        variant="success"
                        type={"submit"}>
                    Update
                </Button>

                <br></br> <br></br> <br></br> <br></br> <br></br> <br></br> <br></br>
                <Button name="delete_1"
                        onClick={() => this.handleDelete()}
                        variant="success"
                        type={"submit"}>
                    Delete
                </Button>


                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default PatientForm;
