import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'
import Caregivers from './caregiver-data/caregiver/caregivers'
import Medications from './medication-data/medication/medications'
import Patients from './patient-data/patient/patients'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/persons'
                            render={() => <Persons/>}
                        />

                        <Route
                            exact
                            path='/caregivers'
                            render={() => <Caregivers/>}
                            />

                        <Route
                            exact
                            path='/medications'
                            render={() => <Medications/>}
                        />
                        <Route
                            exact
                            path='/patients'
                            render={() => <Patients/>}
                        />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
