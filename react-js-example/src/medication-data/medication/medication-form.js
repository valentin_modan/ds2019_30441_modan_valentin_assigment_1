import React from 'react';
import validate from "./validators/medication-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class MedicationForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {

                id: {
                    value: '',
                    placeholder: 'Medication id (used for update/delete)',
                    valid: false,
                    touched: false,

                },

                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                dosage: {
                    value: '',
                    placeholder: 'What is the dosage?...',
                    valid: false,
                    touched: false,
                },

                sideEffects: {
                    value: '',
                    placeholder: 'Side effects?...',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerMedication(medication) {
        return API_USERS.postMedication(medication, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updateMedication(medication) {
        return API_USERS.putMedication(medication, (result, status, error) => {
            console.log(result);

            if (result != null && (status === 200 || status === 2010)) {
                console.log("Succesfully updated medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    deleteMedication(medication) {
        return API_USERS.deleteMedication(medication, (result, status, error) => {
            console.log(result);

            if (result != null && (status === 200 || status === 201)) {
                console.log("Succesfully delete medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }


    handleSubmit() {
        console.log("New medication data:");
        console.log("Id: " + this.state.formControls.id.value);
        console.log("Name: " + this.state.formControls.name.value);


        let user = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value,
        };

        this.registerMedication(user);
    }

    handleUpdate() {
        console.log("New medication data:");
        console.log("Id: " + this.state.formControls.id.value);
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Email: " + this.state.formControls.dosage.value);


        let user = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value,
        };

        this.updateMedication(user);
    }

    handleDelete() {
        console.log("New medication data:");
        console.log("Id: " + this.state.formControls.id.value);
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Dosage: " + this.state.formControls.dosage.value);


        let user = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value,
        };

        this.deleteMedication(user);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>Insert new medication</h1>
                <p> Name: </p>
                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />

                <p> Dossage: </p>
                <TextInput name="dosage"
                           placeholder={this.state.formControls.dosage.placeholder}
                           value={this.state.formControls.dosage.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.dosage.touched}
                           valid={this.state.formControls.dosage.valid}
                />


                <p> SideEffects: </p>
                <TextInput name="sideEffects"
                           placeholder={this.state.formControls.sideEffects.placeholder}
                           value={this.state.formControls.sideEffects.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.sideEffects.touched}
                           valid={this.state.formControls.sideEffects.valid}
                />


                <p></p>
                <Button variant="success"
                        type={"submit"}
                        value="insert">

                    Submit
                </Button>

                <br></br>
                <Button name="update_1"
                        onClick= { () => this.handleUpdate()}
                        variant="success"
                        type={"submit"}>
                    Update
                </Button>

                <br></br> <br></br> <br></br> <br></br> <br></br> <br></br> <br></br>
                <Button name="delete_1"
                        onClick= { () => this.handleDelete()}
                        variant="success"
                        type={"submit"}>
                    Delete
                </Button>


                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default MedicationForm;
