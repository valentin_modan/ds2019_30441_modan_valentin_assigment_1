import React from 'react';
import validate from "./validators/caregiver-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class CaregiverForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {

                id: {
                    value: '',
                    placeholder: 'Person id (used for update/delete)',
                    valid: false,
                    touched: false,

                },

                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                birthDate: {
                    value: '',
                    placeholder: 'What is your birth date?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: false
                    }
                },

                gender: {
                    value: '',
                    placeholder: 'Gender..',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,

                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerCaregiver(caregiver) {
        return API_USERS.postCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updateCaregiver(caregiver) {
        return API_USERS.putCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if (result != null && (status === 200 || status === 2010)) {
                console.log("Succesfully updated person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    deleteCaregiver(caregiver) {
        return API_USERS.deleteCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if (result != null && (status === 200 || status === 201)) {
                console.log("Succesfully delete caregiver with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }


    handleSubmit() {
        console.log("New caregiver data:");
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Address: " + this.state.formControls.address.value);
        console.log("Birth: " + this.state.formControls.birthDate.value);
        console.log("Gender: " + this.state.formControls.gender.value);

        let user = {
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value
        };

        this.registerCaregiver(user);
    }

    handleUpdate() {
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Address: " + this.state.formControls.address.value);
        console.log("Birth: " + this.state.formControls.birthDate.value);
        console.log("Gender: " + this.state.formControls.gender.value);

        let user = {
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value
        };

        this.updateCaregiver(user);
    }

    handleDelete() {
        console.log("Delete:")
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Address: " + this.state.formControls.address.value);
        console.log("Birth: " + this.state.formControls.birthDate.value);
        console.log("Gender: " + this.state.formControls.gender.value);

        let user = {
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value
        };

        this.deleteCaregiver(user);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>Insert new person</h1>
                <p> Name: </p>

                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />

                <p> Address: </p>
                <TextInput name="address"
                           placeholder={this.state.formControls.address.placeholder}
                           value={this.state.formControls.address.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.address.touched}
                           valid={this.state.formControls.address.valid}
                />

                <p> Birth: </p>
                <TextInput name="birthDate"
                           placeholder={this.state.formControls.birthDate.placeholder}
                           value={this.state.formControls.birthDate.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.birthDate.touched}
                           valid={this.state.formControls.birthDate.valid}
                />

                <p> Gender: </p>
                <TextInput name="gender"
                           placeholder={this.state.formControls.gender.placeholder}
                           value={this.state.formControls.gender.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.gender.touched}
                           valid={this.state.formControls.gender.valid}
                />
                <p></p>
                <Button variant="success"
                        type={"submit"}
                        value="insert">

                    Submit
                </Button>

                <br></br>
                <Button name="update_1"
                        onClick= { () => this.handleUpdate()}
                        variant="success"
                        type={"submit"}>
                    Update
                </Button>

                <br></br> <br></br> <br></br> <br></br> <br></br> <br></br> <br></br>
                <Button name="delete_1"
                        onClick= { () => this.handleDelete()}
                        variant="success"
                        type={"submit"}>
                    Delete
                </Button>


                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default CaregiverForm;
